/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConcurrentQueue.h
 * Author: pnookala
 *
 * Created on March 12, 2018, 3:50 PM
 */

#ifndef CONCURRENTQUEUE_H
#define CONCURRENTQUEUE_H

#include <type_traits>
#include <memory>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <typeinfo>
#include <vector>
#include <stack>
#include <thread>
#include "common.h"

template<typename T>
class ConcurrentQueue {
private:
    mutable std::mutex m;
    int capacity;
    T* data; /* all tasks */
    int rear; /* tasks[rear % capacity] is the last item */
    int front; /* tasks[(front+1) % capacity] is the first */
    sem_t task_sem; /* counts available tasks in queue */
    sem_t spaces_sem; /* counts available task slots */
    int numTasks;
public:

    ConcurrentQueue(const int& size) : capacity(size) {
        data = (T*) malloc(sizeof (T) * size);
        rear = 0;
        front = 0;
        sem_init(&task_sem, 0, 0);
        sem_init(&spaces_sem, 0, capacity);
        memset(data, 0, sizeof (T) * size);
    }

    ConcurrentQueue(const ConcurrentQueue& other) {
        std::lock_guard<std::mutex> lock(other.m);

        // copy in constructor body rather than 
        // the member initializer list 
        // in order to ensure that the mutex is held across the copy.
        data = other.data;
        capacity = other.capacity;
        rear = other.rear;
        front = other.front;
        sem_init(&task_sem, 0, 0);
        sem_init(&spaces_sem, 0, capacity);
    }

    void push(T new_value) {
        if (full()) {
            std::lock_guard<std::mutex> lock(m);
            int next = front + 1; //faking pop here
            T cur = data[next % capacity]; //Equivalent to calling pop()
            new_value->next = cur;
            data[next % capacity] = new_value;
            return;
        } else {
            sem_wait(&spaces_sem);
            std::lock_guard<std::mutex> lock(m);
            data[(++rear) % (capacity)] = new_value;
            sem_post(&task_sem);
        }
    }

    T pop() {
        if (full()) {
            std::lock_guard<std::mutex> lock(m);
            int next = front + 1;
            T task = data[next % capacity];
            if(task->next) {
                data[next % capacity] = (T)task->next;
            }
            else {
                front++; //processed all chained tasks, so increment front
                sem_post(&spaces_sem);
            }
            return task;
        } else if(empty()) {
            pthread_testcancel();
            return NULL;
        }
        else {

            sem_wait(&task_sem);
            std::lock_guard<std::mutex> lock(m);
            /* WARNING: THIS MAY NOT WORK AS I WANT LATER */
            T task = data[(++front) % capacity];
            sem_post(&spaces_sem);
            return task;
        }
    }

    bool empty() {
        std::lock_guard<std::mutex> lock(m);
        return (front == rear);
    }

    bool full() {
        //std::lock_guard<std::mutex> lock(m);
        int ne = 0;
        sem_getvalue(&spaces_sem, &ne);
                //if there are no spaces in the queue
        if (ne == 0) {
            return true;
        } else
            return false;
    }

};

#endif /* CONCURRENTQUEUE_H */

