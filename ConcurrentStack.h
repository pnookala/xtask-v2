/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   concurrentstack.h
 * Author: pnookala
 * 
 * Created on February 13, 2018, 10:39 AM
 */

#include <exception>
#include <iostream>
#include <memory>
#include <mutex>
#include <stack>
#include <cstring>
#include <vector>
#include <thread>

template<typename T>
class ConcurrentStack {
private:
    mutable std::mutex m;
    std::vector<T> data;
    int capacity;
public:

    ConcurrentStack(int size) {
        data.reserve(size);
        capacity = size;
    }

    ConcurrentStack(std::vector<T> d) {
        data = d;
    }

    ConcurrentStack(const ConcurrentStack& other) {
        std::lock_guard<std::mutex> lock(other.m);

        // copy in constructor body rather than 
        // the member initializer list 
        // in order to ensure that the mutex is held across the copy.
        data = other.data;
    }

    ConcurrentStack& operator=(const ConcurrentStack&) = delete;

    void push(T new_value) {
        std::lock_guard<std::mutex> lock(m);
        data.push_back(new_value);
    }

    T pop() {
        // check for empty before trying to pop value
        if (data.empty()) {
            pthread_testcancel();
            return NULL;
        }
        
        std::lock_guard<std::mutex> lock(m);

        // allocate return value before modifying stack
        T const res((data.back()));
        data.pop_back();

        return res;
    }

    void pop(T& value) {
        std::lock_guard<std::mutex> lock(m);
        if (data.empty()) return;
        value = data.back();
        data.pop_back();
    }

    bool empty() const {
        std::lock_guard<std::mutex> lock(m);
        return data.empty();
    }

    bool full() const {
        std::lock_guard<std::mutex> lock(m);
        return (data.size() == capacity);
    }
};


