XTASK

This is the lock-free version of Xtask which uses a lock-based stack/queue and runtime processing and scheduling is lock-free.

To compile, go to Xtask directory and run "make"

To run, here are the commands:

> ./dist/Debug/GNU-Linux/xtask 1 10 1  ---- <task type>, <N>, <num threads> -> Find the 10th fibonacci number using 1 thread

> ./dist/Debug/GNU-Linux/xtask 2 1 1000 ---- <task type>, <num threads>, <num elements in array to sort> -> Sorts N element array of randomly generated integers


Please keep compiler flags as is to ensure reliable execution, this is still work in progress.