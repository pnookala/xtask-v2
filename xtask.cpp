/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   xtask.cpp
 * Author: pnookala
 * 
 * Created on February 12, 2018, 3:43 PM
 */

#include <future>
#include <vector>
#include <thread>
#include <pthread.h>
#include <stack>
#include "xtask.h"

struct threadData {
    struct worker *w;
    xtask* currentObj;
    int threadId;
};

sem_t donesignal;
pthread_mutex_t mylock = PTHREAD_MUTEX_INITIALIZER;

xtask::xtask() {
    ms = (struct master_state *) malloc(sizeof (struct master_state));
}

xtask::~xtask() {
}

void* xtask_leaf(xtask_task* t) {
    //Check recursively for parents
    xtask_parent* next = (xtask_parent*) t->parent;
    while (next) {
        //get the hold, so no other thread processes it, wait in while loop if some other thread owns it.
        while (!(__sync_bool_compare_and_swap(&((xtask_parent*) next)->onHold, 0, 1))) {
            sched_yield();
        }
        int dc = __sync_sub_and_fetch(&((xtask_parent*) next)->depCounter, 1);
        if (dc != 0) {
            __sync_bool_compare_and_swap(&((xtask_parent*) next)->onHold, 1, 0); //release the hold
            xtask_task *dummy = (xtask_task*) (malloc(sizeof (xtask_task)));

            *dummy = {NULL, NULL, next, NULL};
            return dummy;
        }
        __sync_bool_compare_and_swap(&((xtask_parent*) next)->onHold, 1, 0); //release the hold
        next = (xtask_parent*) next->task->parent;
    }

    sem_post(&donesignal);
    return NULL;
}

void* worker_handler(void *data) {
    threadData* tl = (threadData*) data;
    int threadId = tl->threadId; //Also the core ID to which it is to be attached.
    struct worker *w = tl->w;
//#ifdef HWLOC
//    hwloc_obj_t obj;
//    hwloc_cpuset_t cpuset;
//    obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, my_cpu % NUM_CPUS);
//    cpuset = hwloc_cpuset_dup(obj->cpuset);
//    hwloc_cpuset_singlify(cpuset);
//    hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
//    char *str = NULL;
//    hwloc_cpuset_asprintf(&str, obj->cpuset);
//#else
////Have pthread_attr_t pin the thread from setup method
//    cpu_set_t set;
//    CPU_ZERO(&set);
//    CPU_SET(threadId % NUM_CPUS, &set);
//    pthread_setaffinity_np(pthread_self(), sizeof (set), &set);
//#endif
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    pthread_cleanup_push(free, tl);
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    while (1) {
        //printf("thread: %d\n", threadId);
        xtask_task *task = w->tasks.pop();
        if (!task) continue;

        xtask_task *retTask = (xtask_task*) task->func(task);
        //If a task is returned, we need to push the child tasks.

        if (retTask) {
            xtask_parent *p;
            if (!retTask->parent) {
                int scnt = 0;
                for (xtask_task* t = retTask; t; t = (xtask_task*) t->sibling, scnt++);
                    p = (xtask_parent*) (malloc(sizeof (xtask_parent)));
                    p->task = (xtask_task*) (malloc(sizeof (xtask_task)));
                    *(p->task) = {xtask_leaf, NULL, task->parent, NULL};
                    p->depCounter = scnt;
                    p->onHold = 0;
            }

            if (retTask->func) {
                for (xtask_task* t = retTask; t; t = (xtask_task*) t->sibling) {
                    t->parent = retTask->parent ? retTask->parent : p;
                    tl->currentObj->xtask_realpush(t);
                }
            }
        }//leaf task
        else {
            //Do the post processing here...
            if (task->parent) {
                int dc = __sync_sub_and_fetch(&((xtask_parent*) (task->parent))->depCounter, 1);
                if (dc == 0) {
                    if (((xtask_parent*) task->parent)->task)
                        tl->currentObj->xtask_push(((xtask_parent*) task->parent)->task);
                }
            } else {
                //If no parent, it is the root task
                sem_post(&donesignal);
            }
        }

    }
    pthread_cleanup_pop(1);
    return NULL;
}

void xtask::xtask_setup(int workers, int numTasks, int startWorkers) {
    ms->numWorkers = workers;
    ms->numTasks = numTasks;
    ms->nextQueue = 0;
    sem_init(&donesignal, 0, 0);
    //Change here for multiple single/multiple stacks
    for (int i = 0; i < workers; i++) {
        struct worker *w = (struct worker*)malloc(sizeof(struct worker));
        ms->workers.emplace_back(w);
    }

    if (startWorkers) {
        
        for (int t  = 0; t < workers; t++) {
            struct threadData *tl = (threadData*) malloc(sizeof (threadData));
            cpu_set_t set;
            CPU_ZERO(&set);
            CPU_SET(t % NUM_CPUS, &set);
            
            pthread_attr_t attr;
            pthread_attr_init(&attr);
            pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &set);
            tl->w = ms->workers[t];
            //Change here for single/multiple stacks
            tl->threadId = t;
            tl->currentObj = this;
            pthread_create(&(ms->workers[t]->t), &attr, worker_handler, (void*) tl);
        }
    }
}

void xtask::xtask_cleanup() {
    free(ms);
}

void xtask::xtask_push(xtask_task *task) {
    xtask_parent *p;
    if (!task->parent) {
        int scnt = 0;
        for (xtask_task* t = task; t; t = (xtask_task*) t->sibling, scnt++);
        p = (xtask_parent*) (malloc(sizeof (xtask_parent)));
        p->task = (xtask_task*) (malloc(sizeof (xtask_task)));
        *(p->task) = {xtask_leaf, NULL, NULL, NULL};
        p->depCounter = scnt;
        p->onHold = 0;
    }

    for (xtask_task* t = task; t; t = (xtask_task*) t->sibling) {
        t->parent = task->parent ? task->parent : p;
        xtask_realpush(t);
    }

}

xtask_task* xtask::xtask_pop(int ds) {
    return (ms->workers[ds]->tasks).pop();
}

void xtask::xtask_realpush(xtask_task *task) {
    int next = __sync_fetch_and_add(&ms->nextQueue, 1);
   
    //Change here for single/multiple stacks
    (ms->workers[next % ms->numWorkers]->tasks).push(task);
    //ms->tasks[0].push(task);
    __sync_fetch_and_add(&ms->numTasks, 1);
}

void xtask::xtask_poll(int* totalTasks) {
    sem_wait(&donesignal);
    for (int i = 0; i < ms->numWorkers; i++) {
        pthread_cancel(ms->workers[i]->t);
        pthread_join(ms->workers[i]->t, NULL);
    }

    *totalTasks = ms->numTasks;
    return;
}

